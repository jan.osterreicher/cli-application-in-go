package main

import (
	"flag"
	"fmt"
)

func main() {
	// Pointers for parameters written when started
	appNamePtr := flag.String("app-name", "empty", "Application name")
	appVersionPtr := flag.String("app-version", "empty", "Application version")
	apiPtr := flag.String("api", "empty", "API")
	cacheSizePtr := flag.String("cache-size", "20GB", "Cache size")
	cfgPtr := flag.String("cfg", "empty", "Configuration file")

    	flag.Parse()

	// For debug only
    	fmt.Println("app-name:", *appNamePtr)
    	fmt.Println("app-version:", *appVersionPtr)
    	fmt.Println("api:", *apiPtr)
    	fmt.Println("cache-size:", *cacheSizePtr)
    	fmt.Println("cfg:", *cfgPtr)




	// 1. Initialize new MinIO client and download the NTX manifest file
	//minioClient, err := minio.New(*appNamePtr + ":" + *appVersionPtr, *accessKeyID, *secretAccessKey, true)
        //if err != nil {
        //        log.Fatal(err)
        //}

	// 2. Blobs

	// 3. Start application
	//fmt.Println("running")
}



//Workflow
//1. Download latest S3 app manifest for app-name + app-version
//2. Download app manifest blobs
//3. “Start application” - e.g. print “running”

//Blob manager
//● should ensure blob integrity
//● should not delete blobs that are currently in use
//● should not delete blobs for last used api
//● should periodically recover space by deleting blobs
//  ○ not in latest manifest when latest manifest blobs for last used api has been
//  downloaded
//  ○ when over cache-size and not used
//● (nice to have) prefetch blobs in background for new manifest based on last N previously
//used apis when there is enough space in cache